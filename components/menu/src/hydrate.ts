// Handle making some changes once we've hydrated the menu with JS
// So its default state can be a fallback
// Doing this means if JS is disabled or fails, we still provide a way to get to the menu items
export default (context: HTMLElement) => {
  // We can stop the overflowing menu items from wrapping, as we're going to add in the "More" dropdown
  const menuItems = context.querySelector("#menu-items");

  if (!menuItems) {
    throw new Error("Menu bar not found");
  }

  menuItems.classList.remove("flex-wrap");

  // The mobile menu can be used as we have JS, so we can hide the main menu on smaller screens
  menuItems.classList.remove("flex");
  menuItems.classList.add("hidden", "xs:flex");

  // And make the mobile menu be only hidden above smaller screens
  const mobileMenuWrapper = context.querySelector("#mobile-menu-wrapper");
  if (!mobileMenuWrapper) {
    throw new Error("Mobile menu wrapper not found");
  }
  mobileMenuWrapper.classList.remove("hidden");
  mobileMenuWrapper.classList.add("xs:hidden");
};
