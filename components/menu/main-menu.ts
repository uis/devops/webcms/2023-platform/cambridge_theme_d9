import type { DrupalInstance, OnceFn } from "../../typings/drupal";
import handleOverlayPanels from "./src/handleOverlayPanels";
import hydrate from "./src/hydrate";
import mobileNav from "./src/mobileNav";
import setupMoreDropdownButton from "./src/setupMoreDropdownButton";
declare const Drupal: DrupalInstance;
declare const once: OnceFn;

((Drupal, once) => {
  Drupal.behaviors.mainMenu = {
    attach: (context: HTMLElement) => {
      once("mainMenu", "#menu-wrapper", context).forEach(
        (context: HTMLElement) => {
          // Handle removing the non-hydrated state of the menu
          // As it no longer needs to fallback
          hydrate(context);

          // Handle the hiding and showing of overlay panels on clicking the menu items
          handleOverlayPanels(context);

          // Handle the hiding and showing of the "More" button and its dropdown
          // If required based on the width of the menu items and the viewport
          setupMoreDropdownButton(context);

          // Handle the switching to and from the mobile navigation
          mobileNav(context);
        },
      );
    },
  };
})(Drupal, once);
